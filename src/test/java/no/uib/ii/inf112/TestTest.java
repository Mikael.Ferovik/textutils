package no.uib.ii.inf112;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class TestTest {
	TextAligner aligner = new TextAligner() {

		public String center(String text, int width) {
			if (width > text.length())
				throw new IllegalArgumentException("Width cannot be greater than length of text.");
			int extra = (width - text.length()) / 2;
			return " ".repeat(extra) + text + " ".repeat(extra);
		}

		public String flushRight(String text, int width) {
			// TODO Auto-generated method stub
			return null;
		}

		public String flushLeft(String text, int width) {
			// TODO Auto-generated method stub
			return null;
		}

		public String justify(String text, int width) {
			// TODO Auto-generated method stub
			return null;
		}};
		
	@Test
	void test() {
		assertTrue(true);
	}

	@Test
	void testCenter() {
		assertEquals("  A  ", aligner.center("A", 5));
		assertEquals(" foo ", aligner.center("foo", 5));
		assertEquals("foo", aligner.center("foo", 3));
		Exception exception = assertThrows(IllegalArgumentException.class, () -> {
			aligner.center("foo", 2);
		});

		String expectedMessage = "Width cannot be greater than length of text.";
		String actualMessage = exception.getMessage();

		assertTrue(actualMessage.contains(expectedMessage));

	}
}
